%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% PROGRAMA RESOLUCION DE CICLO %%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Created by: Pablo Hinojosa and Alvaro Sanjuan

% function: Main calculation loop of the backend structure

% lenguage: matlab

function [E_e,T_01,T_02_A,T_02_B,T_04_A,P_04_A,T_04_B,P_04_B,etac_iso_A,etac_iso_B,etac_iso_C,etat_iso_A,etat_iso_B,etat_iso_C,x_g11,...
 pi_t_A,pi_t_B,pi_t_C,Ab_6,xb_CO2,xb_O2,xb_H2O]= CAJA_NEGRA_SINPOST_EJES_offdesign(sang1,n,m,theta_1,pi_c1,x,y,z,ep_c1,ep_c2,ep_c3...
    ,ep_t1,ep_t2,ep_t3,etacc1,por1,delta_P11,Hp1,etam1,etam2,etam3,pi_tob1,T_0b,P_0b,M_0b,rho_0b,n_ejes1,pi_d1,z0)

tic

global M_0 R R1 T_0 P_0 comp compg1 por x_i PM_i PM_f ep_c_A ep_c_B ep_c_C etacc Hp...
       mf nf na T_0ref ep_t_A ep_t_B ep_t_C etam_A etam_B etam_C sang pi_tob Cp_T0 P_01...
       P_02_A P_02_B P_02_C xb_N2 xb_Ar P_03 pi_c_A pi_c_B pi_c_C T_03 P_0_dis M_0_dis Cp_0_dis R1_dis T_03_dis T_0_dis...
       T_01_dis T_02_dis T_02s_dis eta_comp_dis P_02_dis f_1_dis xb_CO2_dis xb_H2O_dis xb_Ar_dis xb_N2_dis xb_O2_dis...
       x1_CO2_dis x1_H2O_dis x1_Ar_dis x1_N2_dis x1_O2_dis P_03_dis T_04_dis P_04_dis T_04s_dis eta_tur_dis ...
       T61_dis T62_dis pi_crit_dis P_6_dis x_i_dis Cp_T0_dis comp_dis P_01_dis mf_dis nf_dis sang_dis por_dis PM_i_dis...
       PM_f_dis delta_P11_dis etam_3_dis compg1_dis pi_tob_dis etacc_dis Hp_dis pi_d_dis 
%Caracteristicas tecnologicas del motor:
pi_d = pi_d1; etacc = etacc1; T_0ref = 298; 
 
pi_tob = pi_tob1;  %si flag=1, el dato es pi_tob, si no es eta_tob

n_ejes = n_ejes1;

if n_ejes == 1
   pi_c_A = pi_c1^(x/100); pi_c_B = 0; pi_c_C = 0;
   ep_c_A = ep_c1; ep_c_B = 0; ep_c_C = 0;
   ep_t_A = ep_t1; ep_t_B = 0; ep_t_C = 0;
   etam_A = etam1; etam_B = 0; etam_C = 0;
   etat_iso_B = 0;  etat_iso_C = 0; 
   etac_iso_B = 0;  etac_iso_C = 0;
elseif n_ejes == 2 
   pi_c_A = pi_c1^(x/100); pi_c_B = pi_c1^(y/100); pi_c_C = 0;
   ep_c_A = ep_c1; ep_c_B = ep_c2; ep_c_C = 0;
   ep_t_A = ep_t1; ep_t_B = ep_t2; ep_t_C = 0;
   etam_A = etam1; etam_B = etam2; etam_C = 0;
   etat_iso_C = 0; etac_iso_C = 0;  
else
   pi_c_A = pi_c1^(x/100); pi_c_B = pi_c1^(y/100); pi_c_C = pi_c1^(z/100);
   ep_c_A = ep_c1; ep_c_B = ep_c2; ep_c_C = ep_c3; 
   ep_t_A = ep_t1; ep_t_B = ep_t2; ep_t_C = ep_t3;
   etam_A = etam1; etam_B = etam2; etam_C = etam3;
end

%----------------------------------------------------------------------------------------------------------------------------------------------

%Variables de entrada (condiciones de vuelo):
T_0 = T_0b; P_0 = P_0b; rho_0 = rho_0b; M_0 = M_0b; T_03=theta_1*T_0;
sang = sang1/100; %porcentaje 
%----------------------------------------------------------------------------------------------------------------------------------------------
%COMPOSICION DEL AIRE:
comp = [1,   0,   0,   0,   1,   1,   0,   0,   0,   0,   1];
%      C02   H2O  CO   H2   O2   N2   OH   NO   O    H    Ar

%COMPOSICION GAS A LA SALIDA DE LA CAMARA DE COMBUSTION 
compg1 = [1,   1,   0,   0,   1,   1,   0,   0,   0,   0,   1];
  %      C02   H2O  CO   H2   O2   N2   OH   NO   O    H    Ar


%CARACTERISTICAS DEL COMBUSTIBLE 
nf = n; mf = m; PM_f = nf*12.01+mf*1; Hp = Hp1;

%Porcentaje en peso (peso molar) [poner ya todas los pesos molares]

PM_i = [44.01, 18.01528, 28.0101, 2.015, 32, 28.01, 17, 30.01, 16, 1.01, 39.95];
%      C02      H2O       CO      H2     O2    N2   OH  NO      O    H    Ar


%PORCENTAJE EN PESO DE NUESTRA COMPOSICION 
por = por1;

%MOLES:  (estos datos deben ser proporcionados por el usuario)
n_i = [por(1)/PM_i(1), por(2)/PM_i(2), por(3)/PM_i(3), por(4)/PM_i(4), por(5)/PM_i(5), por(6)/PM_i(6), ...
    por(7)/PM_i(7), por(8)/PM_i(8), por(9)/PM_i(9), por(10)/PM_i(10), por(11)/PM_i(11)];

n = sum(n_i);

for i=1:11
    if comp(i)==1 
        x_i(i)=n_i(i)/n; 
    else
        x_i(i)=0;
    end
end

%CARACTERISTICAS DEL AIRE (N2, O2, Ar)
R = 8.3143; %(KJ/Kmol K)
R1 = 8.3143/sum(x_i*PM_i')*1e3; %(J/Kg K)
gamma = 1.4;
na = (gamma-1)/gamma;
%----------------------------------------------------------------------------------------------------------------------------------------------

% CALCULO DE DOSADO ESTEQUIOMETRICO 
 
% f_est1 = (sum(n_i.*PM_i))/n/PM_f*3.717/x_i(6)*(nf+mf/4); %f es gasto de aire/gasto de fuel

%Calculo de calores especificos:

Cp_T0 = lib_coefficients_Cp(T_0,comp,x_i);

% RESOLUCION SISTEMA DE ECUACIONES-------------------------------------------------------------------------------------------------------------

%DESPEJE DE VARIABLES PREVIA 

 
xb_N2 = por(6)/100*(1-sang);

xb_Ar = por(11)/100*(1-sang);
 
P_01 = pi_d*P_0;


%------------------------------------------------------------------------------------------------------------------------------------------
% ITERANTES INICIALES Y RESOLUCION PARA 1 EJE
%------------------------------------------------------------------------------------------------------------------------------------------
if n_ejes == 1
    P_02_A = pi_c_A*P_01; P_02_B = 0; P_02_C = 0;
    P_04_B=0;P_04_C=0;
    P_03 = P_02_A*(1-delta_P11);
    T_02_B = 0; T_02_C = 0; T_04_B = 0; T_04_C = 0;
elseif n_ejes == 2
    P_02_A = pi_c_A*P_01; P_02_B = pi_c_B*P_02_A; P_02_C = 0;
    P_04_C=0;
    P_03 = P_02_B*(1-delta_P11);
    T_02_C = 0; T_04_C = 0;
else
    P_02_A = pi_c_A*P_01; P_02_B = pi_c_B*P_02_A; P_02_C = pi_c_C*P_02_B;
    P_03 = P_02_C*(1-delta_P11);
end

if n_ejes == 1
%CONDICIONES INICIALES 
z_iter = iterantes_iniciales_sinpost_ejes1(por,PM_i,P_03,pi_c_A,ep_c_A,ep_t_A,etacc,etam_A,...
    T_0ref,T_03,sang,pi_tob,T_0,P_0,M_0,xb_N2,xb_Ar,PM_f,nf,mf);

W_c_aux_A_0 = 1e-5*z_iter(4);  

T_01_0 = z_iter(1)*1e-3; T_02s_aux_A_0 = z_iter(2)*1e-3; T_02_aux_A_0 = z_iter(3)*1e-3;

f_1_0 = z_iter(5); xb_CO2_0 = z_iter(6); xb_H2O_0 = z_iter(7); xb_O2_0 = z_iter(8);

x1_N2_0 = z_iter(9); x1_Ar_0 = z_iter(10); x1_CO2_0 = z_iter(11); x1_H2O_0 = z_iter(12); x1_O2_0 = z_iter(13);

T_04_aux_A_0 = z_iter(15)*0.001; P_04_A_0 = z_iter(16); T_04s_aux_A_0 = z_iter(14)*0.001;  

T_6_1_0 = z_iter(17)*1e-3; T_6_2_0 = z_iter(17)*1e-3; pi_crit_0 = z_iter(18); P_6_0 = z_iter(19);


z_0 = [T_01_0; T_02_aux_A_0; W_c_aux_A_0; T_02s_aux_A_0; f_1_0; xb_CO2_0; xb_H2O_0; xb_O2_0; x1_N2_0; x1_Ar_0;...
     x1_CO2_0; x1_H2O_0; x1_O2_0; T_04_aux_A_0; P_04_A_0; T_04s_aux_A_0;...
     T_6_1_0; pi_crit_0; P_6_0; T_6_2_0];

z = fsolve(@ecuaciones_motoroptim_sinpostB_ejes1,z_0);

P_6 = z(19);

if P_6 > P_0

else 
    z = fsolve(@ecuaciones_motoroptim_sinpostA_ejes1,z_0);
end

T_01 = z(1)/0.001; T_02_A = z(2)/0.001; We_c_A = z(3)/0.00001; T_02s_A = z(4)/0.001;

f_1 = z(5); xb_CO2 = z(6); xb_H2O = z(7); xb_O2 = z(8);

x1_N2 = z(9); x1_Ar = z(10); x1_CO2 = z(11); x1_H2O = z(12); x1_O2 = z(13);

T_04_A = z(14)/0.001; P_04_A = z(15); T_04s_A = z(16)/0.001; 

T_6_1 = z(17)/0.001;  pi_crit = z(18); P_6 = z(19); T_6_2 = z(20)/0.001; 


%CALCULO DE VARIABLES POSTERIORES NO NECESARIAS DENTRO:


% COMPRESOR: 
etac_iso_A = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01,T_02s_A)/quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01,T_02_A);

%CAMARA DE COMBUSTION 
Cp_g1 = lib_coefficients_Cp(T_03,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]);

x_g11=[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar];

xbaux_g1 = [xb_N2, xb_Ar, xb_CO2, xb_O2, xb_H2O];
 
xb_g1 = sum(xbaux_g1); 

%TURBINA
We_t_A = quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_A,T_03);

pi_t_A = P_04_A/P_03;

pi_t_B = 0;

pi_t_C=0;

etat_iso_A = (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_A,T_03))/...
    (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04s_A,T_03));

%TOBERA 

C_6 = sqrt(2*quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_6_1,T_04_A));

Ab_6 = xb_g1*R1*T_6_1/(P_6*C_6)/1e5;

%CALCULO DE VARIABLES DE SALIDA 

U = sqrt(2*quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_0,T_01));

E_e = (1-sang+f_1)*C_6 - U + Ab_6*(P_6-P_0)*1e5;

TSFC = (f_1)/E_e*3600*1000;

xb_c = (1-sang) + f_1;

eta_prop = E_e*U/(0.5*(xb_c*C_6^2-U^2)+Ab_6*C_6*(P_6-P_0)*1e5);

eta_mot = (0.5*(xb_c*C_6^2-U^2)+Ab_6*C_6*(P_6-P_0)*1e5)/Hp/f_1;

eta_mp = eta_prop*eta_mot;




%------------------------------------------------------------------------------------------------------------------------------------------
% ITERANTES INICIALES Y RESOLUCION PARA 2 EJES
%------------------------------------------------------------------------------------------------------------------------------------------



elseif n_ejes == 2
%CONDICIONES INICIALES 
z_iter = iterantes_iniciales_sinpost_ejes2(por,PM_i,P_03,pi_c_A,pi_c_B,ep_c_A,ep_c_B,ep_t_A,ep_t_B,etacc,etam_A,etam_B,...
    T_0ref,T_03,sang,pi_tob,T_0,P_0,M_0,xb_N2,xb_Ar,PM_f,nf,mf);

W_c_aux_A_0 = 1e-5*z_iter(4); W_c_aux_B_0 = 1e-5*z_iter(7);  

T_01_0 = z_iter(1)*1e-3; T_02s_aux_A_0 = z_iter(2)*1e-3; T_02_aux_A_0 = z_iter(3)*1e-3; 

T_02s_aux_B_0 = z_iter(5)*1e-3; T_02_aux_B_0 = z_iter(6)*1e-3;

f_1_0 = z_iter(8); xb_CO2_0 = z_iter(9); xb_H2O_0 = z_iter(10); xb_O2_0 = z_iter(11);

x1_N2_0 = z_iter(12); x1_Ar_0 = z_iter(13); x1_CO2_0 = z_iter(14); x1_H2O_0 = z_iter(15); x1_O2_0 = z_iter(16);

T_04s_aux_A_0 = z_iter(17)*0.001; T_04_aux_A_0 = z_iter(18)*0.001; P_04_A_0 = z_iter(19);

T_04s_aux_B_0 = z_iter(20)*0.001; T_04_aux_B_0 = z_iter(21)*0.001; P_04_B_0 = z_iter(22); 

T_6_1_0 = z_iter(23)*0.001; T_6_2_0 = z_iter(23)*0.001; pi_crit_0 = z_iter(24); P_6_0 = z_iter(25);


z_0 = [T_01_0; T_02_aux_A_0; W_c_aux_A_0; T_02s_aux_A_0; T_02_aux_B_0; W_c_aux_B_0; T_02s_aux_B_0;...
      f_1_0; xb_CO2_0; xb_H2O_0; xb_O2_0; x1_N2_0; x1_Ar_0;...
      x1_CO2_0; x1_H2O_0; x1_O2_0; T_04_aux_A_0; P_04_A_0; T_04s_aux_A_0; T_04_aux_B_0; P_04_B_0; T_04s_aux_B_0;...
      T_6_1_0; pi_crit_0; P_6_0; T_6_2_0];

z = fsolve(@ecuaciones_motoroptim_sinpostB_ejes2,z_0);

P_6= z(25);

if P_6 > P_0

else 
    z = fsolve(@ecuaciones_motoroptim_sinpostA_ejes2,z_0);
end

T_01 = z(1)/0.001; T_02_A = z(2)/0.001; We_c_A = z(3)/0.00001; T_02s_A = z(4)/0.001;

T_02_B = z(5)/0.001; We_c_B = z(6)/0.00001; T_02s_B = z(7)/0.001;

f_1 = z(8); xb_CO2 = z(9); xb_H2O = z(10); xb_O2 = z(11);

x1_N2 = z(12); x1_Ar = z(13); x1_CO2 = z(14); x1_H2O = z(15); x1_O2 = z(16);

T_04_A = z(17)/0.001; P_04_A = z(18); T_04s_A = z(19)/0.001;

T_04_B = z(20)/0.001; P_04_B = z(21); T_04s_B = z(22)/0.001; 

T_6_1 = z(23)/0.001;  pi_crit = z(24); P_6 = z(25); T_6_2 = z(26)/0.001; 

%CALCULO DE VARIABLES POSTERIORES NO NECESARIAS DENTRO

%COMPRESOR 
etac_iso_A = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01,T_02s_A)/quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01,T_02_A);

etac_iso_B = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_02_A,T_02s_B)/quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_02_A,T_02_B);

%CAMARA DE COMBUSTION 
Cp_g1 = lib_coefficients_Cp(T_03,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]);

x_g11=[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar];

xbaux_g1 = [xb_N2, xb_Ar, xb_CO2, xb_O2, xb_H2O];
 
xb_g1 = sum(xbaux_g1); 

%TURBINA:
We_t_A = quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_A,T_03);

We_t_B = quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_B,T_04_A);

pi_t_A = P_04_A/P_03;

pi_t_B = P_04_B/P_04_A;

pi_t_C=0;

etat_iso_A = (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_A,T_03))/...
    (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04s_A,T_03));

etat_iso_B = (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_B,T_04_A))/...
    (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04s_B,T_04_A));

%TOBERA 

C_6 = sqrt(2*quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_6_1,T_04_B));

Ab_6 = xb_g1*R1*T_6_1/(P_6*C_6)/1e5;

%CALCULO DE VARIABLES DE SALIDA 

U = sqrt(2*quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_0,T_01));

E_e = (1-sang+f_1)*C_6 - U + Ab_6*(P_6-P_0)*1e5;

TSFC = (f_1)/E_e*3600*1000;

xb_c = (1-sang) + f_1;

eta_prop = E_e*U/(0.5*(xb_c*C_6^2-U^2)+Ab_6*C_6*(P_6-P_0)*1e5);

eta_mot = (0.5*(xb_c*C_6^2-U^2)+Ab_6*C_6*(P_6-P_0)*1e5)/Hp/f_1;

eta_mp = eta_prop*eta_mot;


%------------------------------------------------------------------------------------------------------------------------------------------
% ITERANTES INICIALES Y RESOLUCION PARA 3 EJES
%------------------------------------------------------------------------------------------------------------------------------------------

else
    
%CONDICIONES INICIALES 
z_iter = iterantes_iniciales_sinpost_ejes3(por,PM_i,P_03,pi_c_A,pi_c_B,pi_c_C,ep_c_A,ep_c_B,ep_c_C,ep_t_A,ep_t_B,ep_t_C,etacc,etam_A,etam_B,...
    etam_C,T_0ref,T_03,sang,pi_tob,T_0,P_0,M_0,xb_N2,xb_Ar,PM_f,nf,mf);

W_c_aux_A_0 = 1e-5*z_iter(4); W_c_aux_B_0 = 1e-5*z_iter(7); W_c_aux_C_0 = 1e-5*z_iter(10);  
 
T_01_0 = z_iter(1)*1e-3; T_02s_aux_A_0 = z_iter(2)*1e-3; T_02_aux_A_0 = z_iter(3)*1e-3; 

T_02s_aux_B_0 = z_iter(5)*1e-3; T_02_aux_B_0 = z_iter(6)*1e-3;

T_02s_aux_C_0 = z_iter(8)*1e-3; T_02_aux_C_0 = z_iter(9)*1e-3;

f_1_0 = z_iter(11); xb_CO2_0 = z_iter(12); xb_H2O_0 = z_iter(13); xb_O2_0 = z_iter(14);

x1_N2_0 = z_iter(15); x1_Ar_0 = z_iter(16); x1_CO2_0 = z_iter(17); x1_H2O_0 = z_iter(18); x1_O2_0 = z_iter(19);

T_04s_aux_A_0 = z_iter(20)*0.001;  T_04_aux_A_0 = z_iter(21)*0.001; P_04_A_0 = z_iter(22); 

T_04s_aux_B_0 = z_iter(23)*0.001;  T_04_aux_B_0 = z_iter(24)*0.001; P_04_B_0 = z_iter(25);

T_04s_aux_C_0 = z_iter(26)*0.001;  T_04_aux_C_0 = z_iter(27)*0.001; P_04_C_0 = z_iter(28); 

T_6_1_0 = z_iter(29)*1e-3; T_6_2_0 = z_iter(29)*1e-3; pi_crit_0 = z_iter(30); P_6_0 = z_iter(31);


z_0 = [T_01_0; T_02_aux_A_0; W_c_aux_A_0; T_02s_aux_A_0; T_02_aux_B_0; W_c_aux_B_0; T_02s_aux_B_0; T_02_aux_C_0; W_c_aux_C_0; T_02s_aux_C_0;...
      f_1_0; xb_CO2_0; xb_H2O_0; xb_O2_0; x1_N2_0; x1_Ar_0;
      x1_CO2_0; x1_H2O_0; x1_O2_0; T_04_aux_A_0; P_04_A_0; T_04s_aux_A_0; T_04_aux_B_0; P_04_B_0; T_04s_aux_B_0;
      T_04_aux_C_0; P_04_C_0; T_04s_aux_C_0;...
      T_6_1_0; pi_crit_0; P_6_0; T_6_2_0];

z = fsolve(@ecuaciones_motoroptim_sinpostB_ejes3,z_0);

P_6 = z(31);

if P_6 > P_0

else 
    z = fsolve(@ecuaciones_motoroptim_sinpostA_ejes3,z_0);
end

T_01 = z(1)/0.001; T_02_A = z(2)/0.001; We_c_A = z(3)/0.00001; T_02s_A = z(4)/0.001;

T_02_B = z(5)/0.001; We_c_B = z(6)/0.00001; T_02s_B = z(7)/0.001;

T_02_C = z(8)/0.001; We_c_C = z(9)/0.00001; T_02s_C = z(10)/0.001;

f_1 = z(11); xb_CO2 = z(12); xb_H2O = z(13); xb_O2 = z(14);

x1_N2 = z(15); x1_Ar = z(16); x1_CO2 = z(17); x1_H2O = z(18); x1_O2 = z(19);

T_04_A = z(20)/0.001; P_04_A = z(21); T_04s_A = z(22)/0.001;

T_04_B = z(23)/0.001; P_04_B = z(24); T_04s_B = z(25)/0.001; 

T_04_C = z(26)/0.001; P_04_C = z(27); T_04s_C = z(28)/0.001; 

T_6_1 = z(29)/0.001;  pi_crit = z(30); P_6 = z(31); T_6_2 = z(32)/0.001; 

%CALCULO DE VARIABLES POSTERIORES NO NECESARIAS DENTRO

%COMPRESOR 
etac_iso_A = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01,T_02s_A)/quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01,T_02_A);

etac_iso_B = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_02_A,T_02s_B)/quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_02_A,T_02_B);

etac_iso_C = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_02_B,T_02s_C)/quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_02_B,T_02_C);

%CAMARA DE COMBUSTION 
Cp_g1 = lib_coefficients_Cp(T_03,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]);

x_g11=[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar];

xbaux_g1 = [xb_N2, xb_Ar, xb_CO2, xb_O2, xb_H2O];
 
xb_g1 = sum(xbaux_g1); 

%TURBINA:
We_t_A = quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_A,T_03);

We_t_B = quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_B,T_04_A);

We_t_C = quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_C,T_04_B);

pi_t_A = P_04_A/P_03;

pi_t_B = P_04_B/P_04_A;

pi_t_C = P_04_C/P_04_B;

etat_iso_A = (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_A,T_03))/...
    (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04s_A,T_03));

etat_iso_B = (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_B,T_04_A))/...
    (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04s_B,T_04_A));

etat_iso_C = (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_C,T_04_B))/...
    (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04s_C,T_04_B));

%TOBERA: 

C_6 = sqrt(2*quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_6_1,T_04_B));

Ab_6 = xb_g1*R1*T_6_1/(P_6*C_6)/1e5;

%CALCULO DE VARIABLES DE SALIDA 

U = sqrt(2*quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_0,T_01));

E_e = (1-sang+f_1)*C_6 - U + Ab_6*(P_6-P_0)*1e5;

TSFC = (f_1)/E_e*3600*1000;

xb_c = (1-sang) + f_1;

eta_prop = E_e*U/(0.5*(xb_c*C_6^2-U^2)+Ab_6*C_6*(P_6-P_0)*1e5);

eta_mot = (0.5*(xb_c*C_6^2-U^2)+Ab_6*C_6*(P_6-P_0)*1e5)/Hp/f_1;

eta_mp = eta_prop*eta_mot;

end

% ComprobaciÃ³n de balance energÃ©tico en el sistema:

% T1 = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_0ref,T_01)
% T2 = f_1*Hp*etacc
% T3 = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_0ref,T_02_A)*sang
% T4 = quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_0ref,T_04_A)*xb_g1
% T5 = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01,T_02_A)*(1-etam_A)/etam_A

% K = T1+T2-T3-T4-T5
% pause

%SACAMOS RESULTADOS: 
fprintf('\n')
fprintf('\n')
fprintf('CONDICIONES DE VUELO:')
fprintf('\n')
fprintf('\n')
fprintf('z0 = %10.5f ft',z0/0.3048)
fprintf('\n')
fprintf('T_0 = %10.5f K',T_0)
fprintf('\n')
fprintf('P_0 = %10.5f bar',P_0)
fprintf('\n')
fprintf('rho_0 = %10.5f Kg/m^3',rho_0)
fprintf('\n')
fprintf('\n')

fprintf('DIFUSOR:')
fprintf('\n')
fprintf('\n')
fprintf('T_01 = %10.5f K',T_01)
fprintf('\n')
fprintf('P_01 = %10.5f bar',P_01)

fprintf('\n')
fprintf('\n')
fprintf('COMPRESOR BAJA:')
fprintf('\n')
fprintf('\n')
fprintf('P_02_A = %10.5f bar',P_02_A)
fprintf('\n')
fprintf('T_02_A = %10.5f K',T_02_A)
fprintf('\n')
fprintf('T_02s_A = %10.5f K',T_02s_A)
fprintf('\n')
fprintf('We_c_A = %10.5f KJ/Kg',We_c_A/1000)
fprintf('\n')
fprintf('eta_c_iso_A = %10.5f [-]',etac_iso_A)
if n_ejes == 2
 fprintf('\n')
fprintf('\n')
fprintf('COMPRESOR MEDIA:') 
fprintf('\n')
fprintf('P_02_B = %10.5f bar',P_02_B)
fprintf('\n')
fprintf('T_02_B = %10.5f K',T_02_B)
fprintf('\n')
fprintf('T_02s_B = %10.5f K',T_02s_B)
fprintf('\n')
fprintf('We_c_B = %10.5f KJ/Kg',We_c_B/1000)
fprintf('\n')
fprintf('eta_c_iso_B = %10.5f [-]',etac_iso_B)
elseif n_ejes==3
fprintf('\n')
fprintf('COMPRESOR MEDIA:')  
fprintf('\n')
fprintf('P_02_B = %10.5f bar',P_02_B)
fprintf('\n')
fprintf('T_02_B = %10.5f K',T_02_B)
fprintf('\n')
fprintf('T_02s_B = %10.5f K',T_02s_B)
fprintf('\n')
fprintf('We_c_B = %10.5f KJ/Kg',We_c_B/1000)
fprintf('\n')
fprintf('eta_c_iso_B = %10.5f [-]',etac_iso_B)
fprintf('\n')
fprintf('\n')
fprintf('COMPRESOR ALTA:')
fprintf('\n')
fprintf('P_02_C = %10.5f bar',P_02_C)
fprintf('\n')
fprintf('T_02_C = %10.5f K',T_02_C)
fprintf('\n')
fprintf('T_02s_C = %10.5f K',T_02s_C)
fprintf('\n')
fprintf('We_c_C = %10.5f KJ/Kg',We_c_C/1000)
fprintf('\n')
fprintf('eta_c_iso_C = %10.5f [-]',etac_iso_C)
else
end

fprintf('\n')
fprintf('\n')
fprintf('CAMARA DE COMBUSTION:')
fprintf('\n')
fprintf('\n')
fprintf('sang = %10.5f Kg/s',sang)
fprintf('\n')
fprintf('f_1 = %10.5f [-]',f_1)
fprintf('\n')
fprintf('xb_1_N2 = %10.5f Kg/s',xb_N2)
fprintf('\n')
fprintf('xb_1_Ar = %10.5f Kg/s',xb_Ar)
fprintf('\n')
fprintf('xb_1_CO2 = %10.5f Kg/s',xb_CO2)
fprintf('\n')
fprintf('xb_1_H2O = %10.5f Kg/s',xb_H2O)
fprintf('\n')
fprintf('xb_1_O2 = %10.5f Kg/s',xb_O2)
fprintf('\n')
fprintf('x1_N2 = %10.5f [-]',x1_N2)
fprintf('\n')
fprintf('x1_Ar = %10.5f [-]',x1_Ar)
fprintf('\n')
fprintf('x1_CO2 = %10.5f [-]',x1_CO2)
fprintf('\n')
fprintf('x1_H2O = %10.5f [-]',x1_H2O)
fprintf('\n')
fprintf('x1_O2 = %10.5f [-]',x1_O2)
fprintf('\n')
fprintf('Cp_g1 = %10.5f J/Kg K',Cp_g1)
fprintf('\n')
fprintf('P_03 = %10.5f bar',P_03)
fprintf('\n')
fprintf('\n')
fprintf('TURBINA ALTA:')
fprintf('\n')
fprintf('\n')
fprintf('T_04_A = %10.5f K',T_04_A)
fprintf('\n')
fprintf('T_04s_A = %10.5f K',T_04s_A)
fprintf('\n')
fprintf('P_04_A = %10.5f bar',P_04_A)
fprintf('\n')
fprintf('We_t_A = %10.5f KJ/Kg ',We_t_A/1000)
fprintf('\n')
fprintf('pi_t_A = %10.5f [-]',pi_t_A)
fprintf('\n')
fprintf('eta_t_iso_A = %10.5f [-]',etat_iso_A)

if n_ejes == 2
fprintf('\n')
fprintf('\n')
fprintf('TURBINA MEDIA:')
fprintf('\n')
fprintf('T_04_B = %10.5f K',T_04_B)
fprintf('\n')
fprintf('T_04s_B = %10.5f K',T_04s_B)
fprintf('\n')
fprintf('P_04_B = %10.5f bar',P_04_B)
fprintf('\n')
fprintf('We_t_B = %10.5f KJ/Kg ',We_t_B/1000)
fprintf('\n')
fprintf('pi_t_B = %10.5f [-]',pi_t_B)
fprintf('\n')
fprintf('eta_t_iso_B = %10.5f [-]',etat_iso_B)
fprintf('\n')

elseif n_ejes==3
fprintf('\n')
fprintf('\n')
fprintf('TURBINA MEDIA:')
fprintf('\n')
fprintf('T_04_B = %10.5f K',T_04_B)
fprintf('\n')
fprintf('T_04s_B = %10.5f K',T_04s_B)
fprintf('\n')
fprintf('P_04_B = %10.5f bar',P_04_B)
fprintf('\n')
fprintf('We_t_B = %10.5f KJ/Kg ',We_t_B/1000)
fprintf('\n')
fprintf('pi_t_B = %10.5f [-]',pi_t_B)
fprintf('\n')
fprintf('eta_t_iso_B = %10.5f [-]',etat_iso_B)
fprintf('\n')
fprintf('\n')
fprintf('\n')
fprintf('TURBINA BAJA:')
fprintf('\n')
fprintf('T_04_C = %10.5f K',T_04_C)
fprintf('\n')
fprintf('T_04s_C = %10.5f K',T_04s_C)
fprintf('\n')
fprintf('P_04_C = %10.5f bar',P_04_C)
fprintf('\n')
fprintf('We_t_C = %10.5f KJ/Kg ',We_t_C/1000)
fprintf('\n')
fprintf('pi_t_C = %10.5f [-]',pi_t_C)
fprintf('\n')
fprintf('eta_t_iso_C = %10.5f [-]',etat_iso_C)
else
end

fprintf('\n')
fprintf('\n')
fprintf('TOBERA CONVERGENTE:')
fprintf('\n')
fprintf('\n')
fprintf('T_6 = %10.5f K',T_6_2)
fprintf('\n')
if flag == 0
fprintf('T_6s = %10.5f K',T_6_s2)
fprintf('\n')
else
end
fprintf('pi_crit = %10.5f [-]',pi_crit)
fprintf('\n')
fprintf('P_6 = %10.5f bar',P_6)
fprintf('\n')
fprintf('C_6 = %10.5f m/s',C_6)
fprintf('\n')
fprintf('Ab_6 = %10.5f m^2/kg/s',Ab_6) 
fprintf('\n')

if P_6 > P_0
    fprintf('Tobera bloqueada') 
else
    fprintf('Tobera adaptada') 
end

fprintf('\n')
fprintf('\n')
fprintf('VARIABLES DE SALIDA:')
fprintf('\n')
fprintf('\n')
fprintf('E_e = %10.5f N/(kg/s)',E_e)
fprintf('\n')
fprintf('TSFC = %10.5f kg/kN/h',TSFC)
fprintf('\n')
fprintf('eta_prop = %10.5f ',eta_prop)
fprintf('\n')
fprintf('eta_mp = %10.5f ',eta_mp)
fprintf('\n')
fprintf('eta_mot = %10.5f ',eta_mot)
fprintf('\n')
fprintf('xb_c = %10.5f kg/s ',xb_c)


%FALTA PONER UN INDICADOR DE SI ESTA BLOQUEADA O ADAPTADA
t = toc;
fprintf('\n')
fprintf('\n')
fprintf('Tiempo total de simulacion: %10.5f seg',t)


P_0_dis = P_0; M_0_dis = M_0; Cp_0_dis = Cp_T0; x_i_dis = x_i; R1_dis = R1; T_03_dis = T_03; T_0_dis = T_0; T_01_dis = T_01; 

T_02_dis = T_02_A; T_02s_dis= T_02s_A; eta_comp_dis = etac_iso_A; P_02_dis = P_02_A; f_1_dis = f_1; 

xb_CO2_dis = xb_CO2;  xb_H2O_dis = xb_H2O; xb_Ar_dis = xb_Ar; xb_N2_dis = xb_N2; xb_O2_dis = xb_O2;

x1_CO2_dis = x1_CO2;  x1_H2O_dis = x1_H2O; x1_Ar_dis = x1_Ar; x1_N2_dis = x1_N2; x1_O2_dis = x1_O2;

P_03_dis = P_03; T_04_dis = T_04_A; P_04_dis = P_04_A; T_04s_dis = T_04s_A; eta_tur_dis = etat_iso_A;  

T61_dis = T_6_1; T62_dis  = T_6_2; pi_crit_dis = pi_crit; P_6_dis = P_6; Cp_T0_dis = Cp_T0; comp_dis = comp;

P_01_dis = P_01; mf_dis = mf; nf_dis = nf; sang_dis = sang; por_dis = por; PM_i_dis = PM_i; PM_f_dis = PM_f;

delta_P11_dis = delta_P11; etam_3_dis = etam_A; compg1_dis = compg1; pi_tob_dis = pi_tob; etacc_dis = etacc; Hp_dis = Hp; pi_d_dis = pi_d;
%----------------------------------------------------------------------------------------------------------------------------------------------

end


% programmed by: Pablo Hinojosa ans Alvaro Sanjuan

% Main combustion ecuations

function F = ecuaciones_motoroptim_conpostB_ejes1(z)


global M_0 R1 T_0 comp compg1 compg2 por x_i PM_i PM_f ep_c_A etacc etapc Hp...
       mf nf T_0ref T_03 T_05 mp_a ep_t_A etam_A sang pi_tob Cp_T0 P_01...
       P_02_A  mp_N2 mp_Ar P_03 pi_c_A 
   

T_01_aux = z(1); T_02_aux_A = z(2); W_c_aux_A = z(3); T_02s_aux_A = z(4);

f_1 = z(5); mp_CO2 = z(6); mp_H2O = z(7); mp_O2 = z(8);  

x1_N2 = z(9); x1_Ar = z(10); x1_CO2 = z(11); x1_H2O = z(12); x1_O2 = z(13);

T_04_aux_A = z(14); P_04_A = z(15); T_04s_aux_A = z(16); 

f_2aux = z(17); mp2_CO2 = z(18); mp2_H2O = z(19); mp2_O2 = z(20);

x2_N2 = z(21); x2_Ar = z(22); x2_CO2 = z(23); x2_H2O = z(24); x2_O2 = z(25); P_05 = z(26);

T_6_1 = z(27);  pi_crit = z(28); P_6 = z(29); T_6_2 = z(30); 
 
%ETAPA 1: DIFUSOR + COMPRESOR %--------------------------------------------------------------------------------------------------------

F(1) = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_0,T_01_aux/0.001) - 1/2*(M_0^2*R1*T_0*Cp_T0/(Cp_T0-R1));

F(2) = quadgk(@(T)lib_coefficients_Cp(T,comp,x_i)./T,T_01_aux/0.001,T_02_aux_A/0.001) - R1/ep_c_A*log(pi_c_A);

F(3) = W_c_aux_A/0.00001 - quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_01_aux/0.001,T_02_aux_A/0.001);

F(4) =  quadgk(@(T)lib_coefficients_Cp(T,comp,x_i)./T,T_01_aux/0.001,T_02s_aux_A/0.001) - R1*log(P_02_A/P_01);

%ETAPA 2: CÃ�MARA DE COMBUSTIÃ“N %-------------------------------------------

F(5) = etacc*Hp*f_1-(1+f_1)*quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_0ref,T_03)...
    + quadgk(@(T)lib_coefficients_Cp(T,comp,x_i),T_0ref,T_02_aux_A/0.001);

F(6) = mp_CO2-por(1)/100*mp_a*(1-sang)-PM_i(1)*(f_1*mp_a*(1-sang)/PM_f*nf);

F(7) = mp_H2O-PM_i(2)*f_1*mp_a*(1-sang)/PM_f*mf/2;

F(8) = mp_O2-PM_i(5)*(por(5)/100*mp_a*(1-sang)/PM_i(5)-f_1*mp_a*(1-sang)/PM_f*(nf+mf/4));

F(9) = x1_N2-(mp_N2/PM_i(6))/(sum([mp_N2 mp_Ar mp_CO2 mp_H2O mp_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(10) = x1_Ar-(mp_Ar/PM_i(11))/(sum([mp_N2 mp_Ar mp_CO2 mp_H2O mp_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(11) = x1_CO2-(mp_CO2/PM_i(1))/(sum([mp_N2 mp_Ar mp_CO2 mp_H2O mp_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(12) = x1_H2O-(mp_H2O/PM_i(2))/(sum([mp_N2 mp_Ar mp_CO2 mp_H2O mp_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(13) = x1_O2-(mp_O2/PM_i(5))/(sum([mp_N2 mp_Ar mp_CO2 mp_H2O mp_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

%ETAPA 3: TURBINA --------------------------------------------------------------------------------------------------------------------------

F(14) = R1*ep_t_A*log(P_04_A/P_03) - (quadgk(@(T)lib_coefficients_Cp(T,compg1,...
    [x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar])./T,T_03,T_04_aux_A/0.001));

F(15) = W_c_aux_A/0.00001 - etam_A*(1-sang)*(1+f_1)*(quadgk(@(T)lib_coefficients_Cp(T,compg1,...
    [x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_04_aux_A/0.001,T_03));

F(16) = (quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar])./T,T_03,T_04s_aux_A/0.001))...
    - R1*log(P_04_A/P_03);


%ETAPA 4: POSCOMBUSTOR-------------------------------------------------------------------------------------------------------------------------

F(17) = etapc*Hp*f_2aux-(1+f_2aux)*quadgk(@(T)lib_coefficients_Cp(T,compg2,[x2_CO2,x2_H2O,0,0,x2_O2,x2_N2,0,0,0,0,x2_Ar]),T_0ref,T_05)...
    + quadgk(@(T)lib_coefficients_Cp(T,compg1,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_0ref,T_04_aux/0.001);

F(18) = mp2_CO2-mp_CO2-PM_i(1)*f_2aux*(1-sang)*(mp_a)*(1+f_1)/PM_f*nf; %sacar factor comun

F(19) = mp2_H2O-mp_H2O-PM_i(2)*f_2aux*(1-sang)*(mp_a)*(1+f_1)/PM_f*mf/2;

F(20) = mp2_O2-PM_i(5)*(mp_O2/PM_i(5)-f_2aux*(1-sang)*(mp_a)*(1+f_1)/PM_f*(nf+mf/4));

F(21) = x2_N2-(mp2_N2/PM_i(6))/(sum([mp2_N2 mp2_Ar mp2_CO2 mp2_H2O mp2_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(22) = x2_Ar-(mp2_Ar/PM_i(11))/(sum([mp2_N2 mp2_Ar mp2_CO2 mp2_H2O mp2_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(23) = x2_CO2-(mp2_CO2/PM_i(1))/(sum([mp2_N2 mp2_Ar mp2_CO2 mp2_H2O mp2_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(24) = x2_H2O-(mp2_H2O/PM_i(2))/(sum([mp2_N2 mp2_Ar mp2_CO2 mp2_H2O mp2_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(25) = x2_O2-(mp2_O2/PM_i(5))/(sum([mp2_N2 mp2_Ar mp2_CO2 mp2_H2O mp2_O2]./[PM_i(6) PM_i(11)...
    PM_i(1) PM_i(2) PM_i(5)]));

F(26) = P_05 - P_04*(1-0.08*mp_g1^2/mp_g1max^2);


%TOBERA (CONVERGENTE):------------------------------------------------------------------------------------------------------------------------

F(27) = 1 - 2*(lib_coefficients_Cp(T_6_1/0.001,compg2,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar])-R1)*...
        quadgk(@(T)lib_coefficients_Cp(T,compg2,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]),T_6_1/0.001,T_05)/...
        (R1*T_6_1/0.001*lib_coefficients_Cp(T_6_1/0.001,compg2,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar]));

F(28) = quadgk(@(T)lib_coefficients_Cp(T,compg2,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar])./T,T_6_1/0.001,T_05)-R1*log(1/pi_tob*pi_crit);

%           if pi_crit < P_04/P_0
%           bloq = 1; %tobera bloqueada!
          F(29) = P_6 - P_04_A/pi_crit;
          F(30) = T_6_1 - T_6_2;
            
%           else
%           bloq = 0; %tobera adaptada
%             F(29) = P_6 - P_0;
%             F(30) = quadgk(@(T)lib_coefficients_Cp(T,compg2,[x1_CO2,x1_H2O,0,0,x1_O2,x1_N2,0,0,0,0,x1_Ar])./T,T_6_2/0.001,T_05)...
%                 -R1*log(1/pi_tob*P_05/P_0);            
%           end
   

end




